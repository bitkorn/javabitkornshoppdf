/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.bitkorn.bitkornshoppdf.pdf;

import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author allapow
 */
public class AddressesBasket {

    private Connection conn = null;
    private static final Logger LOG4J = Logger.getRootLogger();

    private boolean valid = false;
    private final HashMap shipment = new HashMap();
    private final HashMap invoice = new HashMap();

    private final String querySelectUserAddress = "SELECT sua.*, ic.*"
            + " FROM shop_user_address sua"
            + " LEFT JOIN iso_country ic USING (iso_country_id)"
            + " WHERE sua.user_id = ?"
            + " ORDER BY sua.shop_user_address_id ASC";

    private final String querySelectBasketAddress = "SELECT sba.*, ic.*"
            + " FROM shop_basket_address sba"
            + " LEFT JOIN iso_country ic USING (iso_country_id)"
            + " WHERE sba.shop_basket_unique = ?"
            + " ORDER BY sba.shop_basket_address_id ASC";

    /**
     *
     * @param conn
     * @param basketUnique
     * @param userId
     */
    public AddressesBasket(Connection conn, String basketUnique, int userId) {
        this.conn = conn;
        if (!computeBasketAddresses(basketUnique)) {
            if(computeUserAddresses(userId)) {
                valid = true;
            }
        } else {
            valid = true;
        }
    }

    private boolean computeBasketAddresses(String basketUnique) {
        try {
            PreparedStatement stmtQueryAdress = this.conn.prepareStatement(querySelectBasketAddress);
            stmtQueryAdress.setString(1, basketUnique);
            ResultSet resultAddresses = stmtQueryAdress.executeQuery();
            if (!resultAddresses.first()) {
//                LOG4J.error("Basket hat keine Adresse. basketUnique: " + basketUnique);
                return false;
            }
            shipment.put("name", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_name1") + " " + resultAddresses.getString("shop_basket_address_name2")));
            shipment.put("street", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_street") + " " + resultAddresses.getString("shop_basket_address_street_no")));
            shipment.put("city", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_zip") + " " + resultAddresses.getString("shop_basket_address_city")));
            shipment.put("country", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("iso_country_iso") + " - " + resultAddresses.getString("iso_country_name")));
            shipment.put("email", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_email")));
            shipment.put("tel", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_tel")));
            if (resultAddresses.next()) {
                invoice.put("name", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_name1") + " " + resultAddresses.getString("shop_basket_address_name2")));
                invoice.put("street", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_street") + " " + resultAddresses.getString("shop_basket_address_street_no")));
                invoice.put("city", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_zip") + " " + resultAddresses.getString("shop_basket_address_city")));
                invoice.put("country", resultAddresses.getString("iso_country_iso") + " - " + resultAddresses.getString("iso_country_name"));
                invoice.put("email", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_email")));
                invoice.put("tel", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_basket_address_tel")));
            }
            return true;
        } catch (SQLException ex) {
            LOG4J.error(ex.getLocalizedMessage());
        }
        return false;
    }

    private boolean computeUserAddresses(int userId) {
        if(userId == 0) {
            return false;
        }
        try {
            PreparedStatement stmtQueryAdress = this.conn.prepareStatement(querySelectUserAddress);
            stmtQueryAdress.setInt(1, userId);
            ResultSet resultAddresses = stmtQueryAdress.executeQuery();
            if (!resultAddresses.first()) {
                LOG4J.error("User hat keine Adresse. user_id: " + userId);
                return false;
            }
            shipment.put("name", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_name1") + " " + resultAddresses.getString("shop_user_address_name2")));
            shipment.put("street", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_street") + " " + resultAddresses.getString("shop_user_address_street_no")));
            shipment.put("city", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_zip") + " " + resultAddresses.getString("shop_user_address_city")));
            shipment.put("country", resultAddresses.getString("iso_country_iso") + " - " + resultAddresses.getString("iso_country_name"));
            shipment.put("email", "");
            shipment.put("tel", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_tel")));
            if (resultAddresses.next()) {
                invoice.put("name", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_name1") + " " + resultAddresses.getString("shop_user_address_name2")));
                invoice.put("street", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_street") + " " + resultAddresses.getString("shop_user_address_street_no")));
                invoice.put("city", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_zip") + " " + resultAddresses.getString("shop_user_address_city")));
                invoice.put("country", resultAddresses.getString("iso_country_iso") + " - " + resultAddresses.getString("iso_country_name"));
                invoice.put("email", "");
                invoice.put("tel", StringEscapeUtils.unescapeHtml4(resultAddresses.getString("shop_user_address_tel")));
            }
            return true;
        } catch (SQLException ex) {
            LOG4J.error(ex.getLocalizedMessage());
        }
        return false;
    }

    public boolean isValid() {
        return valid;
    }
    
    public boolean existAddressInvoice() {
        return !invoice.isEmpty();
    }

    public HashMap getShipment() {
        return shipment;
    }

    public HashMap getInvoice() {
        return invoice;
    }
    
    
}
