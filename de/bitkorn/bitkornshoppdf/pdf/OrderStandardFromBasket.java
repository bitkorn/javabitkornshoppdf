package de.bitkorn.bitkornshoppdf.pdf;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import de.bitkorn.bitkornshoppdf.BitkornShopPdf;
import de.bitkorn.bitkornshoppdf.db.GetConfig;
import de.bitkorn.bitkornshoppdf.event.FooterStandard;
import de.bitkorn.bitkornshoppdf.event.HeaderStandard;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Set;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author allapow
 */
public class OrderStandardFromBasket {

    private Connection conn = null;
    private static final Logger LOG4J = Logger.getRootLogger();

    private final String querySelectBasket = "SELECT "
            + "    sbi.shop_basket_item_id,"
            + "    sbi.shop_article_id,"
            + "    sbi.shop_article_amount,"
            + "    sbi.shop_basket_item_article_options,"
            + "    sb.*,"
            + "    sa.shop_article_sku,"
            + "    sa.shop_article_type,"
            + "    sa.shop_article_size_def_id,"
            + "    sa.shop_article_weight,"
            + "    sa.shop_article_shipping_costs_single,"
            + "    sa.shop_article_price,"
            + "    sa.shop_article_tax,"
            + "    FORMAT(sbi.shop_article_amount * sa.shop_article_price, 2) AS shop_article_price_total,"
            + "    sa.shop_article_name,"
            + "    sa.shop_article_desc,"
            + "    sa.shop_article_active,"
            + "    sa.shop_article_stock_using,"
            + "    sdo.*"
            + " FROM"
            + "    shop_basket_item sbi"
            + "        LEFT JOIN"
            + "    shop_basket sb USING (shop_basket_unique)"
            + "        LEFT JOIN"
            + "    shop_article sa USING (shop_article_id)"
            + "        LEFT JOIN"
            + "    shop_document_order sdo USING (shop_basket_unique)"
            + " WHERE"
            + "    sb.shop_basket_unique = ?";

    /**
     *
     * @param conn
     */
    public OrderStandardFromBasket(Connection conn) {
        this.conn = conn;
    }

    /**
     *
     * @param fqfn
     * @param shopBasketUnique
     * @return
     */
    public int createPdf(String fqfn, String shopBasketUnique) {
        try {
            PreparedStatement stmtQueryBasket = this.conn.prepareStatement(querySelectBasket);
            stmtQueryBasket.setString(1, shopBasketUnique);
            ResultSet resultBasket = stmtQueryBasket.executeQuery();
            if (resultBasket.first()) {
                Document document = new Document(PageSize.A4, BitkornShopPdf.MARGIN_LEFT, BitkornShopPdf.MARGIN_RIGHT, BitkornShopPdf.MARGIN_TOP, BitkornShopPdf.MARGIN_BOTTOM); // Margin gilt dem Content, nicht header und footer!
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fqfn));
//                HeaderStandard headerStandard = new HeaderStandard();
//                writer.setPageEvent(new HeaderStandard());
//                writer.setPageEvent(new FooterStandard());
                document.open();
                document.addCreationDate();
                document.addCreator(GetConfig.getAppConfigValue("shop_owner"));
                document.addAuthor(GetConfig.getAppConfigValue("shop_owner"));
                document.addTitle(GetConfig.getAppConfigValue("shop_name"));
                document.addSubject("Order");
//                document.setMargins(BitkornShopPdf.MARGIN_LEFT, BitkornShopPdf.MARGIN_RIGHT, BitkornShopPdf.MARGIN_TOP, BitkornShopPdf.MARGIN_BOTTOM);
                PdfContentByte canvas = writer.getDirectContent();

                /**
                 * Letterhead
                 */
                PdfReader readerLetterhead = new PdfReader(BitkornShopPdf.LETTERHEAD_FQN);
                PdfImportedPage page01 = writer.getImportedPage(readerLetterhead, 1);
                canvas.addTemplate(page01, 0, 0);

                /**
                 * User Address
                 */
                AddressesBasket addresses = new AddressesBasket(conn, shopBasketUnique, resultBasket.getInt("user_id"));
                if(!addresses.isValid()) {
                    LOG4J.error("Es gibt keine Adressen. user_id: " + resultBasket.getInt("user_id") + "; BasketUnique: " + shopBasketUnique);
                    return -1;
                }
                HashMap<String, String> address;
                if(addresses.existAddressInvoice()) {
                    address = addresses.getInvoice();
                } else {
                    address = addresses.getShipment();
                }
                Phrase phraseAddress = new Phrase();
                phraseAddress.setFont(BitkornShopPdf.DARK_GREY_M);
                phraseAddress.add(address.get("name"));
                phraseAddress.add(Chunk.NEWLINE);
                phraseAddress.add(address.get("street"));
                phraseAddress.add(Chunk.NEWLINE);
                phraseAddress.add(address.get("city"));
                phraseAddress.add(Chunk.NEWLINE);
                phraseAddress.add(address.get("country"));
                phraseAddress.add(Chunk.NEWLINE);
                ColumnText textAddress = new ColumnText(canvas);
                textAddress.setSimpleColumn(phraseAddress,
                        BitkornShopPdf.LETTER_WINDOW_LEFT, BitkornShopPdf.LETTER_WINDOW_LEFT_Y,
                        BitkornShopPdf.LETTER_WINDOW_WIDTH, BitkornShopPdf.LETTER_WINDOW_RIGHT_Y,
                        BitkornShopPdf.FONT_SIZE_L, Element.ALIGN_LEFT);
                textAddress.go();

                /**
                 * Ueberschrift (Rechnung 12345)
                 */
                Phrase phraseInvoiceH1 = new Phrase();
                phraseInvoiceH1.setFont(BitkornShopPdf.DARK_GREY_L_BOLD);
                phraseInvoiceH1.add("Bestellung " + resultBasket.getString("shop_document_order_number"));
                ColumnText textInvoiceH1 = new ColumnText(canvas);
                textInvoiceH1.setSimpleColumn(phraseInvoiceH1,
                        BitkornShopPdf.MARGIN_LEFT, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(95),
                        BitkornShopPdf.mmToUu(120), PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(85),
                        BitkornShopPdf.FONT_SIZE_L, Element.ALIGN_LEFT);
                textInvoiceH1.go();

                /**
                 * Anrede und Einleitung
                 */
                Phrase phraseIntroduction = new Phrase();
                phraseIntroduction.setFont(BitkornShopPdf.DARK_GREY_M);
                phraseIntroduction.add("Sehr geehrte/r " + address.get("name") + ",");
                phraseIntroduction.add(Chunk.NEWLINE);
                phraseIntroduction.add(Chunk.NEWLINE);
                phraseIntroduction.add(GetConfig.getAppConfigValue("pdf_order_introduction"));
                ColumnText textIntroduction = new ColumnText(canvas);
                textIntroduction.setSimpleColumn(phraseIntroduction,
                        BitkornShopPdf.MARGIN_LEFT, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(120),
                        PageSize.A4.getWidth() - BitkornShopPdf.MARGIN_RIGHT, PageSize.A4.getHeight() - BitkornShopPdf.mmToUu(100),
                        BitkornShopPdf.FONT_SIZE_M, Element.ALIGN_LEFT);
                textIntroduction.go();

                /**
                 * fuers table.setSpacingBefore()
                 * (ab hier nur noch relative Positionen)
                 */
                Paragraph paragraph = new Paragraph(" "); // MUSS wenigstens n Blank drin sein!!!
                document.add(paragraph);
                /**
                 * Tabelle mit Produktlisting
                 */
                PdfPTable table = new PdfPTable(3);
                table.setWidthPercentage(90);
                table.setHorizontalAlignment(0);
                table.setSpacingBefore(BitkornShopPdf.mmToUu(70));
                int[] colWidths = {10, 70, 20};
                table.setWidths(colWidths);
                PdfPCell cell;
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Anzahl", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Artikel", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Preis", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(cell);
                /*
                 * Produktlisting
                 */
                ArticleOptions articleOptions = new ArticleOptions(conn);
                Set<String> optionStringsSet;
                do {
                    cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("shop_article_amount"), BitkornShopPdf.DARK_GREY_M));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                    cell = new PdfPCell();
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.addElement(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("shop_article_name"), BitkornShopPdf.DARK_GREY_M));
                    if (articleOptions.computeArticleOptions(resultBasket.getString("shop_basket_item_article_options")).isValid()) {
                        for (String optionString : articleOptions.getOptionStringsSet()) {
//                            cell.addElement(Chunk.NEWLINE);
                            cell.addElement(new Phrase(BitkornShopPdf.FONT_SIZE_S, optionString, BitkornShopPdf.DARK_GREY_S));
                        }
                    }
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("shop_article_price_total").replace('.', ',') + " €", BitkornShopPdf.DARK_GREY_M));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);
                    table.completeRow();
                } while (resultBasket.next());

                resultBasket.last();

                // Summe Artikel
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Summe", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setColspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("computed_value_article_price_total_sum").replace('.', ',') + " €", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                table.completeRow();
                
                if(resultBasket.getFloat("shop_basket_discount_value") > 0) {
                    cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Rabatt (" + resultBasket.getString("shop_basket_discount_hash") + ")", BitkornShopPdf.DARK_GREY_M));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setColspan(2);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("shop_basket_discount_value").replace('.', ',') + " €", BitkornShopPdf.DARK_GREY_M_BOLD));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);
                    table.completeRow();
                }

                // Versandkosten
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Versandkosten", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setColspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("computed_value_article_shipping_costs_computed").replace('.', ',') + " €", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                table.completeRow();

                // Umsatzsteuer
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Umsatzsteuer inkl.", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setColspan(2);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, resultBasket.getString("computed_value_shop_basket_tax_total_sum_end").replace('.', ',') + " €", BitkornShopPdf.DARK_GREY_M));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                table.completeRow();

                // Gesamtkosten
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, "Gesamtkosten", BitkornShopPdf.DARK_GREY_M_BOLD));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setColspan(2);
                table.addCell(cell);
                Float sumTotal = resultBasket.getFloat("computed_value_article_price_total_sum") + resultBasket.getFloat("computed_value_article_shipping_costs_computed");
                cell = new PdfPCell(new Phrase(BitkornShopPdf.FONT_SIZE_M, String.format("%.2f", sumTotal) + " €", BitkornShopPdf.DARK_GREY_M_BOLD));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                table.completeRow();
                
                document.add(table);

                /**
                 * Payment Infos
                 * Ein Paragraph wird relativ zum vorigen Objekt gesetzt.
                 */
                if (resultBasket.getString("payment_method").equalsIgnoreCase("prepay")) {
                    Paragraph paragraphPaymentinfo = new Paragraph();
                    paragraphPaymentinfo.setSpacingBefore(60f);
                    paragraphPaymentinfo.setFont(BitkornShopPdf.DARK_GREY_M);
                    paragraphPaymentinfo.add(GetConfig.getAppConfigValue("pdf_order_payment_prepay_text"));
                    paragraphPaymentinfo.add(Chunk.NEWLINE);
                    paragraphPaymentinfo.setFont(BitkornShopPdf.DARK_GREY_M_BOLD);
                    paragraphPaymentinfo.add("Bezahlungskennung: " + resultBasket.getString("payment_number"));
                    document.add(paragraphPaymentinfo);
                }
                /*
                 * prevent "IOException: The document has no pages" error which often occurs when the document contains no meaningful
                 */
//                document.add(new Chunk("leerer Chunk"));
                document.close();
            }
        } catch (SQLException | DocumentException | FileNotFoundException ex) {
            LOG4J.error(ex.getLocalizedMessage());
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(OrderStandardFromBasket.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 1;
    }

}
