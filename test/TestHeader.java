package test;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class TestHeader extends PdfPageEventHelper {

    private static final org.apache.log4j.Logger LOG4J = org.apache.log4j.Logger.getRootLogger();

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        PdfContentByte canvas = writer.getDirectContent();
        ColumnText testColumn = new ColumnText(canvas);
        Phrase testPhrase = new Phrase();
        testPhrase.setFont(new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.DARK_GRAY));
        testPhrase.add("testTextHeader"); // only after setFont(), setFont() has effect!!!
        testColumn.setSimpleColumn(testPhrase,
                100, PageSize.A4.getHeight() - 100,
                200, PageSize.A4.getHeight() - 60,
                6, Element.ALIGN_LEFT);

        try {
            testColumn.go();
        } catch (DocumentException ex) {
            LOG4J.error(ex.getMessage());
        }
    }
}
